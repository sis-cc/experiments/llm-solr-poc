# PoC
The scope of this project is the support for **Natural Language Search through Large Language Models**. 

Large Language Models can be used to learn the structure of documents in Apache Solr and map a natural language query to such structure, to automatically expand it and offering a transparent experience to users.

The proof-of-concept (PoC) code and configurations that were designed and developed serve as a search RESTful API.  
This accepts a natural language query as input, interacts with a large language model to obtain the most relevant document information (topics and dimensions) based on the query, uses the response obtained from the model to formulate a Solr query, and then executes this query to return relevant results/data streams together with the relevant filter values chosen by the GPT model.

## Repository content
- **[example-queries](example-queries)**: contains some example queries executed via Postman
- **[src](src)**: is the main folder containing the source code of the PoC
  - **[base52Encoding](src/base52Encoding)**: contains the python script responsible for calling an external web server to perform the base52 encoding of dimensions

During the PoC execution logs file are generated inside the logs directory of the main project folder.

## Requirements

Create an environment (using conda or other virtual environment) with Python version 3.10:
```bash
conda create --name PoC-OECD python=3.10

conda activate PoC-OECD

pip install -r requirements.txt

brew install node
```
N.B. these work for a unix-based Operative Systems like MacOs

## Configuration file

Before running the application, it may be necessary to adjust the configuration file.

This [configuration file](./src/config.py) defines several key variables used in the application:

- **SOLR_ADDRESS**: is the address to a Solr instance. In this PoC it is the address of the development (dev) instance that points to the Solr core named "sease" on the specified IP address and port. Leave it as it is if you want to use the dev instance.


- **OPENAI_API_KEY**: API key acts as a unique identifier and authentication method for the user to interact with OpenAI's services. In this PoC it is the API key created using the "oecd-poc1@sease.io" OpenAI account. If a new account is created with the related API key, you must replace the API key here.


- **LARGE_LANGUAGE_MODEL**: is the id of the model being used from OpenAI. In this PoC, it refers to the "gpt-3.5-turbo-instruct" model, which is a variant of the GPT-3.5 model optimized for instructive tasks. (It is recommended not to change it)

## Run
From the `src` folder:
```bash
flask --app main run
```

Server running on:
```
http://127.0.0.1:5000
```

HTTP Request (GET):
```
http://127.0.0.1:5000/query=<natural_language_query>

Example:
http://127.0.0.1:5000/query=What%20were%20the%20sulfur%20oxide%20emissions%20in%20Australia%20in%202013
```

[example-queries](example-queries) contains some example queries that can be executed via Postman.