import requests
import utils
from collections import OrderedDict
import logging


def get_field_list_as_csv(solr_url):
    """
    Fetches data from Solr in CSV format.
    """
    query_params = {
        'fl': '*_en_ss',
        'wt': 'csv',
        'rows': 0}
    response = requests.get(solr_url, params=query_params)
    if response.status_code == 200:
        fields = response.text.strip()
        fields_list = fields.split(',')
        return fields_list
    else:
        logging.info('- - - - ERROR: %s', response.status_code)
        return None


def general_field_facet_query(solr, en_ss_fields):
    # Solr - Field facet on topics, dimensions list and dimensions' fields
    params = {
        'facet': 'true',
        'facet.field': en_ss_fields,
        'facet.mincount': 1,
        'rows': 0
    }
    results = utils.extract_facet_values(solr.search('*:*', **params))
    field_facet_dict = dict(zip(en_ss_fields, results))
    return field_facet_dict


def create_query_from_dicts(generative_answer, extractive_answer):
    # create the query with all the terms in OR
    generative_keywords = " ".join(generative_answer)
    generative_keywords_no_duplicates = ' '.join(OrderedDict.fromkeys(generative_keywords.split()))
    main_query = 'name_sfs_text_en:(' + generative_keywords_no_duplicates + ')'

    # create the pf, pf2 and pf3 parts
    all_substrings = []
    for item in generative_answer:
        all_substrings.extend(utils.generate_substrings(item))
    all_substrings_no_duplicates = list(OrderedDict.fromkeys(all_substrings))
    phrase_query = ' OR '.join(['name_sfs_text_en:"{}"'.format(item) for item in all_substrings_no_duplicates])

    extractive_dimensions_keywords = " OR ".join([f"\"{dimension}\"" for dimension in extractive_answer[
        "features"]])
    extractive_dimensions_query_part = f"dimensions_en_ss:({extractive_dimensions_keywords})"

    extractive_topics_keywords = " OR ".join([f"\"{topic}\"" for topic in extractive_answer[
        "topics"]])
    extractive_topics_query_part = f"srQMgwl_en_ss:({extractive_topics_keywords})"

    full_query = f"{main_query} OR {phrase_query} OR {extractive_dimensions_query_part} OR" \
                 f" {extractive_topics_query_part}"
    return full_query


def query_to_identify_dataflow(solr, generative_answer, extractive_answer):
    query = create_query_from_dicts(generative_answer, extractive_answer)
    results = solr.search(query, rows=1)
    dataflow_data = []
    dimensions_lists = []
    for doc in results.docs:
        # add the value of the field if it is present, because some documents may not contain it (as description)
        doc_data = {
            "id": doc["id"],
            "name": doc["name_sfs_text_en"] if "name_sfs_text_en" in doc else "",
            "description": doc["description_sfs_text_en"] if "description_sfs_text_en" in doc else ""
        }
        dataflow_data.append(doc_data)
        dimensions_lists.append(doc["dimensions_en_ss"])

    dimensions_list = utils.create_unique_list(dimensions_lists)
    return dataflow_data, dimensions_list
