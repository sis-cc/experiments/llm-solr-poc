def extract_facet_values(results):
    field_values_lists = []
    for field in results.facets["facet_fields"].values():
        # for multivalued dimensions, the list contains a sequence of: string value, facet cardinality, and so on; we
        # only want to take the string value and remove the integer.
        field_values = [item for item in field if not isinstance(item, int)]
        field_values_lists.append(field_values)
    return field_values_lists


def create_unique_list(list_of_lists):
    result = set(x for l in list_of_lists for x in l)
    return list(result)


def generate_substrings(string):
    words = string.split()

    # pf
    substrings = [string]

    # generate pairs of word shingles (pf2)
    if len(words) > 2:
        for i in range(len(words) - 1):
            substrings.append(' '.join(words[i:i + 2]))

    # generate triplets of word shingles (pf3)
    if len(words) > 3:
        for i in range(len(words) - 2):
            substrings.append(' '.join(words[i:i + 3]))

    return substrings


def rename_keys_in_dict(dictionary, old_keys, new_keys):
    for old, new in zip(old_keys, new_keys):
        dictionary[new] = dictionary[old]
        del dictionary[old]
    return dictionary
