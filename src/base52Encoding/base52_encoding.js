// Import the 'bs52' module
var bs52 = require('bs52');

// Get command line arguments, excluding the first two (node executable and script file path)
var args = process.argv.slice(2);

// Initialize an array to store encoded values
var encodedValues = [];
// Iterate through each command line argument and encode each argument using the 'bs52' module and push the result into the 'encodedValues' array
args.forEach(arg => {
    encodedValues.push(bs52.encode(arg));
});

// Print the array of encoded values to the console
console.log(encodedValues);
