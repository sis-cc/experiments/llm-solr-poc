import subprocess
import ast
import os

script_directory = os.path.dirname(os.path.abspath(__file__))

def base52_encoding(available_dimensions_list):
    command = ["node", script_directory + "/base52_encoding.js"] + available_dimensions_list
    dimensions_values_encoded = (subprocess.run(command, capture_output=True, text=True)).stdout
    dimensions_values_encoded_list = ast.literal_eval(dimensions_values_encoded)
    dimensions_values_encoded_list_suffix_added = [item + "_en_ss" for item in dimensions_values_encoded_list]
    return dimensions_values_encoded_list_suffix_added