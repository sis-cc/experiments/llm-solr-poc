from flask import Flask
import pysolr
import solr_requests
import openAI_requests
from base52Encoding.base52_encoding import base52_encoding
import concurrent.futures
import logging
import os
import time
import json
import utils

PARENT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# creating a Flask app
app = Flask(__name__)
app.config.from_object('config.Config')
openAI_requests.set_app_config(app)

# Solr configuration
solr_address = app.config['SOLR_ADDRESS']
# Create a Solr instance
solr_instance = pysolr.Solr(solr_address)

# Solr - Get the list of all *_en_ss fields
en_ss_fields_list = solr_requests.get_field_list_as_csv(solr_address + '/select')

# Solr - Field facet on topics (srQMgwl_en_ss), dimensions list (dimensions_en_ss) and dimensions' fields (*_en_ss)
en_ss_fields_facet_dict = solr_requests.general_field_facet_query(solr_instance, en_ss_fields_list)


@app.route('/query=<natural_language_query>')
def search(natural_language_query):
    if not os.path.exists(PARENT_DIR + '/logs'):
        os.makedirs(PARENT_DIR + '/logs')
    time_str = time.strftime("%Y-%m-%d_%H-%M")
    logging.basicConfig(filename=PARENT_DIR + '/logs/OECD_PoC_' + time_str + '.log', filemode='w',
                        format='%(asctime)s %(message)s', level=logging.INFO)

    logging.info('- Natural Language Query: %s\n', natural_language_query)

    logging.info('- PARALLEL REQUESTS TO OPENAI MODEL')
    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        # GENERATIVE REQUEST
        generative_future = executor.submit(openAI_requests.generative_request, natural_language_query)
        # EXTRACTIVE REQUEST
        extractive_future = executor.submit(openAI_requests.extractive_request, natural_language_query,
                                            en_ss_fields_facet_dict["srQMgwl_en_ss"],
                                            en_ss_fields_facet_dict["dimensions_en_ss"])

        generative_answer = generative_future.result()
        extractive_answer = extractive_future.result()

    logging.info('- - - - Generative answer is: %s', generative_answer)
    logging.info('- - - - Extractive answer is: %s\n', extractive_answer)

    logging.info('- SOLR LUCENE QUERY TO IDENTIFY DATAFLOWS')
    dataflow, available_dimensions_list = solr_requests.query_to_identify_dataflow(solr_instance, generative_answer,
                                                                                   extractive_answer)

    logging.info('- - - - Dataflow retrieved is: %s', dataflow)
    logging.info(
        '- - - - {:d} dimensions are available for the above dataflow: {}\n'.format(len(available_dimensions_list),
                                                                                    available_dimensions_list))

    logging.info('- BASE52 ENCODING of dimensions values')
    dimensions_to_not_encode = {"Year", "Time period", "Time"}
    available_dimensions_set = set(available_dimensions_list)
    available_dimensions_to_encode = list(available_dimensions_set - dimensions_to_not_encode)
    available_dimensions_to_not_encode = list(available_dimensions_set & dimensions_to_not_encode)
    dimensions_values_encoded_list = base52_encoding(available_dimensions_to_encode)
    logging.info('- - - - Dimensions values encoded are: %s\n', dimensions_values_encoded_list)

    logging.info('- Retrieve dimensions values from the en_ss field facets\n')
    available_dimension_for_filtering = {key: en_ss_fields_facet_dict[key] for key in dimensions_values_encoded_list if
                                         key in en_ss_fields_facet_dict}
    # Add "Year" and/or "Time period" and/or "Time"
    for dim in available_dimensions_to_not_encode:
        available_dimension_for_filtering[dim] = dim

    logging.info('- REQUEST TO OPENAI MODEL to obtain a JSON of key-value pairs (for filtering)\n')
    utils.rename_keys_in_dict(available_dimension_for_filtering, dimensions_values_encoded_list,
                              available_dimensions_to_encode)

    filtering_answer = openAI_requests.filtering_request(natural_language_query, available_dimension_for_filtering)

    logging.info('- Return a JSON containing both dataflow and filters values')
    json_result = {
        "natural_language_query": natural_language_query,
        "dataflow": dataflow,
        "filters": filtering_answer
    }
    logging.info('- - - - Final JSON: %s\n', json.dumps(json_result))

    return json_result


if __name__ == '__main__':
    app.run(debug=True)
