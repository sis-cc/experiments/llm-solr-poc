import openai
import re
import json
from flask import current_app
import tiktoken
from flask import abort
import random
import openai.error as error
import logging

RESPONSE_MAX_TOKENS = 350
MODEL_MAX_CONTEXT_WINDOW = 4097


def set_app_config(app):
    global API_key, model
    with app.app_context():
        API_key = current_app.config['OPENAI_API_KEY']
        openai.api_key = API_key
        model = current_app.config['LARGE_LANGUAGE_MODEL']


def openAI_request(prompt):
    try:
        response = openai.Completion.create(
            model=model,
            prompt=prompt,
            temperature=0,
            max_tokens=RESPONSE_MAX_TOKENS
        )
    except error.RateLimitError as e:
        abort(429, f"Server Error (TOO MANY REQUESTS): {e.args[0]}")
    text_response = response['choices'][0]["text"].strip()
    return text_response


def generative_request(query):
    prompt = "Return 5 additional relevant terms related to this query \"" \
             + query + "\". Return just the terms that must be relevant and have the same meaning"
    # prompt = "Return the 5 most relevant synonyms related to this query \"" + query + "\""
    # prompt = "Return 5 alternative term queries using synonyms of this query \"" + query + "\". Return just the
    # queries."
    check_prompt_length(prompt)
    response = openAI_request(prompt)
    terms_list = re.findall(r'\d+\.\s*(.+)', response)
    terms_list.append(query)
    return terms_list


def extractive_request(query, topic_list, dimension_list):
    random.shuffle(dimension_list)
    prompt = "Given this JSON representation for a statistic: {\"topics\": " + str(topic_list) + ", \"features\":  " \
             + str(dimension_list) + "}, how would a JSON structure for the query \"" \
             + query + "\" look like? Return in the JSON only the keys present in the JSON representation: " \
                       "topics and features. For each key, choose the most relevant elements taking the exact " \
                       "string from the provided list without making them up or mixing them between lists of " \
                       "different keys. Do not choose more than 2 elements for topics key and more than 5 elements " \
                       "for features key."

    check_prompt_length(prompt)
    response = openAI_request(prompt)
    response_replaced = response.replace("'", "\"")
    response_dict = check_response(response_replaced)
    return response_dict


def filtering_request(query, dimensions_dict):
    keys_list = list(dimensions_dict.keys())
    prompt = "Given this JSON representation for a statistic: " \
             + str(dimensions_dict) + ", how would a JSON structure for the query \"" \
             + query + "\" look like? Return in the JSON only the keys present in the JSON representation: " \
             + str(keys_list) + ". For each key, choose the one most relevant element taking the exact string " \
                                "from the provided list without making it up or mixing it between lists of " \
                                "different keys. For those keys where you do not find a relevant value given the " \
                                "query, return no value."
    check_prompt_length(prompt)
    response = openAI_request(prompt)
    response_replaced = response.replace("'", "\"")
    response_dict = check_response(response_replaced)
    return response_dict


def check_prompt_length(prompt):
    encoding = tiktoken.encoding_for_model(model)
    num_tokens_of_prompt = len(encoding.encode(prompt))
    if (num_tokens_of_prompt + RESPONSE_MAX_TOKENS) > MODEL_MAX_CONTEXT_WINDOW:
        error_message = f"The model {model} with this current free tier exceeds " \
                        f"the " \
                        f"token context window limit in a request!"
        abort(400, description=error_message)
    else:
        return False


def check_response(response):
    if not response.endswith("}"):
        error_message = f"The model {model} with this current free tier exceeds the " \
                        f"token context window limit in a request!"
        abort(400, description=error_message)

    try:
        response_dict = json.loads(response)
        return response_dict
    except ValueError as e:
        logging.info('- - - - INVALID Extractive answer is: %s\n', response)
        abort(500, f"GPT model returns an invalid JSON response: {e.args[0]}")
