# Example queries

Here are the answers of all the example queries in the file **OECD_PoC.postman_collection.json** file.

For each query, there is:
- Data Explorer links (both Demo and OECD) to show what the current results would be with that query.
- HTTP request (of the prototype) executed in Postman.
- GPT Generative answer: the LLM performs a natural language query expansion with synonyms and related terms.
- GPT Extractive answer: the LLM extracts the most relevant topics and dimensions from the list of topics and the list of dimension provided for the given query.
- Dataflow retrieved: with the results obtained from the GPT model answers, we create a Solr query to identify the most relevant dataflow.
- Dimensions available: the Solr query also returns the dimensions available for the above dataflow.
- Final JSON: a JSON of key-value pairs containing information about dataflow retrieved and the filters selected by the GPT model.


### 1) What were the sulfur oxide emissions in Australia in 2013

#### DATA EXPLORER

- Demo .Stat Data Explorer = https://de-demo.siscc.org/?lc=en&tm=What%20were%20the%20sulfur%20oxide%20emissions%20in%20Australia%20in%202013&pg=0

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=What were the sulfur oxide emissions in Australia in 2013`

**GPT Generative answer is**: 

`['Sulfur dioxide emissions', 'Air pollution', 'Environmental impact', 'Fossil fuel combustion', 'Acid rain']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Environment#ENV#|Air and climate#ENV_AC#'], 'dimensions_en_ss': ['Jurisdiction', 'Commodity', 'Time period', 'Pollutant', 'Year']}`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:DF_AIR_EMISSIONS', 'name': 'Emissions of air pollutants', 'description': ''}]`

**4 dimensions are available for the above dataflow**: 

`['Country', 'Year', 'Pollutant', 'Variable']`


**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "",
            "id": "ds-siscc-qa:DF_AIR_EMISSIONS",
            "name": "Emissions of air pollutants"
        }
    ],
    "filters": {
        "Country": [
            "0|Australia#AUS#"
        ],
        "Pollutant": [
            "0|Sulphur Oxides#SOX#"
        ],
        "Variable": [
            "0|Total man-made emissions#TOT#"
        ],
        "Year": "2013"
    },
    "natural_language_query": "What were the sulfur oxide emissions in Australia in 2013"
}
```

### 2) PM10 produced by industrial combustion in EU in 2015

#### DATA EXPLORER

- Demo .Stat Data Explorer = https://de-demo.siscc.org/?lc=en&tm=PM10%20produced%20by%20industrial%20combustion%20in%20EU%20in%202015&pg=0

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=PM10 produced by industrial combustion in EU in 2015`

**GPT Generative answer is**: 

`['Industrial emissions', 'Air pollution', 'Particulate matter', 'Combustion processes', 'European Union (EU) regulations']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Environment#ENV#|Air and climate#ENV_AC#', '1|Economy#ECO#|GDP and spending#ECO_GDP#', '1|Environment#ENV#|Water#ENV_WAT#', '1|Jobs#JOB#|Employment#JOB_EMP#', '1|Economy#ECO#|Productivity#ECO_PRO#'], 'dimensions_en_ss': ['Pollutant', 'Economic activity', 'Country', 'Year', 'Unit of measure']}`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:DF_AIR_EMISSIONS', 'name': 'Emissions of air pollutants', 'description': ''}]`

**4 dimensions are available for the above dataflow**: 

`['Year', 'Variable', 'Pollutant', 'Country']`


**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "",
            "id": "ds-siscc-qa:DF_AIR_EMISSIONS",
            "name": "Emissions of air pollutants"
        }
    ],
    "filters": {
        "Country": [
            "0|European Union (28 countries)#EU28#"
        ],
        "Pollutant": [
            "0|Particulates (PM10)#PM10#"
        ],
        "Variable": [
            "3|Total man-made emissions#TOT#|Total Stationary Sources#STAT_TOT#|Combustion#STAT_COMB#|Industrial combustion#STAT_COMB_IND#"
        ],
        "Year": "2015"
    },
    "natural_language_query": "PM10 produced by industrial combustion in EU in 2015"
}
```

### 3) Full time number of hours worked in a week for women in Denmark

#### DATA EXPLORER

- OECD Data Explorer = https://data-explorer.oecd.org/?tm=Full%20time%20number%20of%20hours%20worked%20in%20a%20week%20for%20women%20in%20Denmark&pg=0

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=Full time number of hours worked in a week for women in Denmark
`

**GPT Generative answer is**: 

`['Weekly working hours for women in Denmark', 'Full-time work schedule for Danish women', 'Number of hours worked per week by female employees in Denmark', "Women's average weekly working hours in Denmark", 'Full-time employment hours for women in Denmark']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Jobs#JOB#|Employment#JOB_EMP#', '0|Society#SOC#', '0|Education#EDU#', '0|Government#GOV#', '0|Energy#NRG#'], 'dimensions_en_ss': ['Worker status', 'Sex', 'Time', 'Employment size class', 'Working time arrangement']}`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:DSD_HW@DF_AVG_USL_WK_WKD', 'name': 'Average usual weekly hours worked on the main job', 'description': 'This table contains data on average usual weekly hours worked in the main job broken down by total employment, full-time employment and part-time employment.  Actual hours of work instead of usual hours of work are only available in some countries (Mexico and Korea).  Data are further broken down by professional status - employees, total employment - by sex and standardised age groups (15-24, 25-54, 55-64, 65+ and total).</p>In order to facilitate analysis and comparisons over time, historical data for OECD members have been provided over as long a period as possible, often even before a country became a member of the Organisation. Information on the membership dates of all OECD countries can be found at <a href="http://www.oecd.org/about/membersandpartners/list-oecd-member-countries.htm">OECD Ratification Dates</a>'}]`

**6 dimensions are available for the above dataflow**:

`['Time period', 'Age', 'Sex', 'Working time arrangement', 'Worker status', 'Reference area']`

**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "This table contains data on average usual weekly hours worked in the main job broken down by total employment, full-time employment and part-time employment.  Actual hours of work instead of usual hours of work are only available in some countries (Mexico and Korea).  Data are further broken down by professional status - employees, total employment - by sex and standardised age groups (15-24, 25-54, 55-64, 65+ and total).</p>In order to facilitate analysis and comparisons over time, historical data for OECD members have been provided over as long a period as possible, often even before a country became a member of the Organisation. Information on the membership dates of all OECD countries can be found at <a href=\"http://www.oecd.org/about/membersandpartners/list-oecd-member-countries.htm\">OECD Ratification Dates</a>",
            "id": "ds-siscc-qa:DSD_HW@DF_AVG_USL_WK_WKD",
            "name": "Average usual weekly hours worked on the main job"
        }
    ],
    "filters": {
        "Age": "0|Total#_T#",
        "Reference area": "0|Denmark#DNK#",
        "Sex": "0|Female#F#",
        "Time period": "Time period",
        "Worker status": "0|Employees#ICSE93_1#",
        "Working time arrangement": "0|Full time#FT#"
    },
    "natural_language_query": "Full time number of hours worked in a week for women in Denmark"
}
```

### 4) part-time working hours in Italy self-employed male

#### DATA EXPLORER

- OECD Data Explorer = https://data-explorer.oecd.org/?tm=part-time%20working%20hours%20in%20Italy%20self-employed%20male&pg=0&snb=3

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=part-time working hours in Italy self-employed male
`

**GPT Generative answer is**: 

`['Part-time employment', 'Working schedule', 'Self-employment', 'Male workers', 'Italian labor market']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Jobs#JOB#|Employment#JOB_EMP#', '0|Society#SOC#', '0|Education#EDU#', '1|Economy#ECO#|Productivity#ECO_PRO#', '1|Jobs#JOB#|Earnings and wages#JOB_EW#'], 'dimensions_en_ss': ['Working time arrangement', 'Country', 'Sex', 'Employment size class', 'Worker status']}`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:DSD_HW@DF_AVG_USL_WK_WKD', 'name': 'Average usual weekly hours worked on the main job', 'description': 'This table contains data on average usual weekly hours worked in the main job broken down by total employment, full-time employment and part-time employment.  Actual hours of work instead of usual hours of work are only available in some countries (Mexico and Korea).  Data are further broken down by professional status - employees, total employment - by sex and standardised age groups (15-24, 25-54, 55-64, 65+ and total).</p>In order to facilitate analysis and comparisons over time, historical data for OECD members have been provided over as long a period as possible, often even before a country became a member of the Organisation. Information on the membership dates of all OECD countries can be found at <a href="http://www.oecd.org/about/membersandpartners/list-oecd-member-countries.htm">OECD Ratification Dates</a>'}]`

**6 dimensions are available for the above dataflow**:

`['Age', 'Time period', 'Sex', 'Working time arrangement', 'Worker status', 'Reference area']`

**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "This table contains data on average usual weekly hours worked in the main job broken down by total employment, full-time employment and part-time employment.  Actual hours of work instead of usual hours of work are only available in some countries (Mexico and Korea).  Data are further broken down by professional status - employees, total employment - by sex and standardised age groups (15-24, 25-54, 55-64, 65+ and total).</p>In order to facilitate analysis and comparisons over time, historical data for OECD members have been provided over as long a period as possible, often even before a country became a member of the Organisation. Information on the membership dates of all OECD countries can be found at <a href=\"http://www.oecd.org/about/membersandpartners/list-oecd-member-countries.htm\">OECD Ratification Dates</a>",
            "id": "ds-siscc-qa:DSD_HW@DF_AVG_USL_WK_WKD",
            "name": "Average usual weekly hours worked on the main job"
        }
    ],
    "filters": {
        "Age": "0|Total#_T#",
        "Reference area": "0|Italy#ITA#",
        "Sex": "0|Male#M#",
        "Time period": "Time period",
        "Worker status": "0|Self-employed#ICSE93_2T5#",
        "Working time arrangement": "0|Part time#PT#"
    },
    "natural_language_query": "part-time working hours in Italy self-employed male"
}
```

### 5) percentage of fresh water resources in Finland in 2020

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=percentage of fresh water resources in Finland in 2020
`

**GPT Generative answer is**: 

`['Proportion of freshwater resources in Finland in 2020', 'Share of freshwater resources in Finland in 2020', 'Amount of freshwater resources in Finland in 2020', 'Portion of freshwater resources in Finland in 2020', 'Quantity of freshwater resources in Finland in 2020']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Environment#ENV#|Water#ENV_WAT#'], 'dimensions_en_ss': ['Reporting country', 'Time Period', 'Year', 'Subject', 'Variable']
}
`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:DF_WATER_RESOURCES', 'name': 'Freshwater resources (long term annual average)', 'description': ''}]`

**3 dimensions are available for the above dataflow**:

`['Country', 'Year', 'Variable']`

**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "",
            "id": "ds-siscc-qa:DF_WATER_RESOURCES",
            "name": "Freshwater resources (long term annual average)"
        }
    ],
    "filters": {
        "Country": [
            "0|Finland#FIN#"
        ],
        "Variable": [
            "0|Freshwater 95% of time#FRESH_95_RESOU#"
        ],
        "Year": "2020"
    },
    "natural_language_query": "percentage of fresh water resources in Finland in 2020"
}
```


### 6) monthly minimum wages in Spain in 2018

#### DATA EXPLORER

- Demo .Stat Data Explorer = https://de-demo.siscc.org/?lc=en&tm=monthly%20minimum%20wages%20in%20Spain%20in%202018&pg=0

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=monthly minimum wages in Spain in 2018`

**GPT Generative answer is**: 

`['Minimum salary', 'Wage floor', 'Monthly pay', "Spain's minimum wage", '2018 salary requirements']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Jobs#JOB#|Employment#JOB_EMP#', '1|Finance#FIN#|Monetary aggregates#FIN_MON#', '1|Government#GOV#|General government#GOV_GG#', '1|Economy#ECO#|GDP and spending#ECO_GDP#', '1|Environment#ENV#|Air and climate#ENV_AC#'], 'dimensions_en_ss': ['Commodity', 'Reference area', 'Adjustment', 'Financing type', 'Aggregation Operation']}`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:DF_MW_CURP', 'name': 'Minimum wages at current prices in NCU', 'description': ''}]`

**3 dimensions are available for the above dataflow**:

`['Pay period', 'Country', 'Time']`

**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "",
            "id": "ds-siscc-qa:DF_MW_CURP",
            "name": "Minimum wages at current prices in NCU"
        }
    ],
    "filters": {
        "Country": [
            "0|Spain#ESP#"
        ],
        "Pay period": [
            "0|Monthly#M#"
        ],
        "Time": "Time"
    },
    "natural_language_query": "monthly minimum wages in Spain in 2018"
}
```

### 7) what was the Gross domestic product of USA in 2022

#### PoC Prototype

**HTTP Request**:

`http://127.0.0.1:5000/query=what was the Gross domestic product of USA in 2022`

**GPT Generative answer is**: 

`['US GDP in 2022', 'United States GDP in 2022', '2022 US GDP', 'GDP of USA in 2022', '2022 GDP of United States']`

**GPT Extractive answer is**: 

`{'srQMgwl_en_ss': ['1|Economy#ECO#|GDP and spending#ECO_GDP#'], 'dimensions_en_ss': ['Time', 'Country', 'Item ID', 'Year', 'Commodity']}`

**Dataflow retrieved is**:

`[{'id': 'ds-siscc-qa:SNA_TABLE1', 'name': '1. Gross domestic product (GDP)', 'description': ''}]`

**4 dimensions are available for the above dataflow**:

`['Country', 'Transaction', 'Measure', 'Year']`

**Final JSON**:

```
{
    "dataflow": [
        {
            "description": "",
            "id": "ds-siscc-qa:SNA_TABLE1",
            "name": "1. Gross domestic product (GDP)"
        }
    ],
    "filters": {
        "Country": "0|United States#USA#",
        "Measure": "0|Gross Domestic Product#GDP#",
        "Transaction": "0|Gross domestic product#B1GQ#",
        "Year": "2022"
    },
    "natural_language_query": "what was the Gross domestic product of USA in 2022"
}
```